package id.mc.solution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution1 {
    public static void main(String... args){
        int[] nums = {3,1,4,2};
        Arrays.sort(nums);
        List<Integer> results = new ArrayList<>();

        for (int num : nums) {
            int belowZero = 0;
            for (int i : nums) {
                int sub = num - i;
                if (sub < 0) {
                    belowZero++;
                }
            }

            if (belowZero <= 0) {
                results.add(num);
            }
        }

        for (Integer result : results) {
            System.out.println(result);
        }
    }
}
