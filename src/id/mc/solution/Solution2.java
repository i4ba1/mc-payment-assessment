package id.mc.solution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

public class Solution2 {
    public static void main(String... args){
        int[] nums = {3,1,4,2};
        Arrays.sort(nums);
        int n = 4;
        Hashtable<String, Integer> data = new Hashtable<>();
        List<Integer> results = new ArrayList<>();

        for (int num : nums) {
            int xDivElement = num / n;
            if (data.get(String.valueOf(xDivElement)) == null) {
                results.add(num);
            }
            data.put(String.valueOf(num), xDivElement);
        }

        for (Integer result : results) {
            System.out.println(result);
        }

    }
}
