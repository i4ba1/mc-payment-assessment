package id.mc.solution;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution3 {
    public static void main(String... args){
        Scanner input = new Scanner(System.in);
        String word = input.nextLine();
        String[] words = word.split(" ");
        int x = input.nextInt();

        List<String> wordList = new ArrayList<>();
        for (String w:words) {
            if (w.length() <= x){
                wordList.add(w);
            }
        }

        for (String s : wordList) {
            System.out.println(s);
        }
    }
}
